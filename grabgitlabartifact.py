#!python
import urllib.request
import urllib.parse
import json
import sys
import os.path as path
import os
import argparse

parser = argparse.ArgumentParser(description='get artifacts from a gitlab job')

parser.add_argument('--ignore_settings', action="store_true", help="Ignore settings files")
parser.add_argument('gitlab_host', type=str, help="resolvable hostname for gitlab server")
parser.add_argument('project', type=str, help="path to project on server in the form of <namespace>/<projectname>")
parser.add_argument('job_name', type=str, help="name of job as specified in CI yaml file.")
parser.add_argument('access_token', type=str, help="A git lab access token usable for specified project")

ARTIFACT_FILE = "/tmp/artifacts.zip"

parser.add_argument('--output_file', type=str, help="path to write artifacts archive. Default is %s" % ARTIFACT_FILE,
                    nargs='?', default=ARTIFACT_FILE)

SETTINGS_FILE_NAME = "~/.grabgitlabartifact/settings.json"

parser.add_argument('--settings_file', type=str, help="path to settings file. Default is %s" % SETTINGS_FILE_NAME,
                    nargs='?',
                    default=SETTINGS_FILE_NAME)

parser.add_argument('--push_url', type=str, nargs='?', help="""instead of downloading archive, just check if an archive 
would be downloaded, then issue a request at the given url", nargs='?""", default="")



args = parser.parse_args()

GITLAB_HOST = args.gitlab_host
GITLAB_TOKEN = args.access_token
GITLAB_PROJECT = args.project
GITLAB_JOB_NAME = args.job_name
SETTINGS_FILE_NAME = path.expanduser(args.settings_file)
ARTIFACT_FILE = path.expanduser(args.output_file)
PUSH_URL = args.push_url

ignore_settings = args.ignore_settings

GITLAB_TOKEN_NAME = "__gitlab_token__"


def load_project_settings(host, project, settings_file_name):
    """Load settings files up to the project level.  I only did this because pycharm said I was duplicating code!

    :param host: resolvable host name for the gitlab server
    :param project: this should be a path to the project on the server in the form of "<namespace>/<projectname>"
    :param settings_file_name: paths to settings file
    :return: the entire settings block
    """
    settings = {}
    try:
        with open(settings_file_name) as f:
            settings = json.load(f)
    except Exception as e:
        print("Couldn't read settings file: %s.\nUsing empty settings." % str(e), file=sys.stderr)
    if host not in settings.keys():
        settings[host] = {}

    if project not in settings[host].keys():
        settings[host][project] = {}
    return settings


def load_settings(host, project, job_name, settings_file_name):
    """Get the settings, if any, from the settings file.  Otherwise returns a blank hash.  Also gets any
    Gitlab access token that's been saved for the project.

    :param host: resolvable host name for the gitlab server
    :param project: this should be a path to the project on the server in the form of "<namespace>/<projectname>"
    :param job_name: the name of the job as given in the gitlab-ci.yaml file.
    :param settings_file_name: paths to settings file
    :return: A dictionary containing any settings loaded from the file, second, any stored gitlab token for the project
    If no settings are found, an new empty dictionary is returned.
    """
    gitlab_token = None

    settings = load_project_settings(host, project, settings_file_name)

    if GITLAB_TOKEN_NAME in settings[host][project].keys():
        gitlab_token = settings[host][project][GITLAB_TOKEN_NAME]

    if job_name not in settings[host][project].keys():
        settings[host][project][job_name] = {}

    return settings[host][project][job_name], gitlab_token


def save_settings(host, project, job_name, settings_dict, settings_file_name, token=None):
    """
    Save settings_dict under [host][project][job_name] in settings_file_name.  Create a new file if necessary.

    If token is not None, then save that too under [host][project]

    Note that this overwrites all existing settings for the job name!  Keys that exist in old settings but not in settings_dict
    will be wiped out.

    :param host: resolvable host name for gitlab server
    :param project: path to the project on server.  "Form is <namespace>/<projectname>"
    :param job_name: the name of the job we're saving settings for.
    :param settings_dict: the settings to save
    :param settings_file_name: path to settings file
    :param token: if not None, then save this token as a settings under project
    :return: None
    """
    settings = load_project_settings(host, project, settings_file_name)

    if token is not None:
        settings[host][project][GITLAB_TOKEN_NAME] = token

    settings[host][project][job_name] = settings_dict

    dirname = path.dirname(settings_file_name)
    if dirname:
        os.makedirs(dirname, exist_ok=True)
    try:
        with open(settings_file_name, "w") as f:
            json.dump(settings, f)
    except Exception as e:
        print("Failed to write new settings: " + str(e), file=sys.stderr)


if not ignore_settings:
    job_settings, tok = load_settings(GITLAB_HOST, GITLAB_PROJECT, GITLAB_JOB_NAME, SETTINGS_FILE_NAME)
else:
    job_settings = {}

last_processed_id = 0
if "last_processed_id" in job_settings.keys():
    last_processed_id = job_settings["last_processed_id"]

GITLAB_PROJECT_QUOTED = urllib.parse.quote(GITLAB_PROJECT, safe='')

jobs_req = urllib.request.Request("https://git.ligo.org/api/v4/projects/%s/jobs" % GITLAB_PROJECT_QUOTED,
                                  headers={"Private-Token": GITLAB_TOKEN})

jobs_resp = urllib.request.urlopen(jobs_req)

jobs_json = jobs_resp.read()

jobs = json.loads(jobs_json.decode("utf-8"))

for job in jobs:
    if job["name"] != GITLAB_JOB_NAME:
        continue

    if job["status"] != "success":
        continue

    try:
        job_id = int(job["id"])
    except ValueError:
        continue



    # we're good! process one job, store the id and quit.
    if PUSH_URL:
        print("Push URL Included. Pushing instead of downloading artifacts.")
        last_pushed_id = 0
        try:
            if "last_pushed_id" in job_settings:
                last_pushed_id = job_settings["last_pushed_id"]
        except ValueError:
            pass
        if job_id <= last_pushed_id:
            print("already pushed the latest job")
            break

        url = PUSH_URL

        parameters = {"gitlabhost": GITLAB_HOST, "project": GITLAB_PROJECT, "jobname": GITLAB_JOB_NAME,
                      "accesstoken": GITLAB_TOKEN}

        if len(parameters) > 0:
            if url.find("?") >= 0:
                url += "&"
            else:
                url += "?"
            url += "&".join(["%s=%s" % (i, parameters[i]) for i in parameters])
        print("pushing to " + url)
        resp = urllib.request.urlopen(url)
        if int(resp.code) == 201:
            print("Pushed to %s and got expected response." % PUSH_URL)
            job_settings["last_pushed_id"] = job_id
        else:
            print("Response code %s was not as expected" % str(resp.code))
            sys.exit(4)
    else:
        if job_id <= last_processed_id:
            print("first job found no later than last processed")
            break

        print("processing job " + str(job_id))

        # save artifacts
        try:
            dirname = path.dirname(ARTIFACT_FILE)
            if dirname:
                os.makedirs(dirname, exist_ok=True)

            with open(ARTIFACT_FILE, "wb") as f:
                art_req = urllib.request.Request(
                    "https://git.ligo.org/api/v4/projects/%s/jobs/%d/artifacts" % (GITLAB_PROJECT_QUOTED, job_id),
                    headers={"Private-Token": GITLAB_TOKEN})

                art_resp = urllib.request.urlopen(art_req)
                f.write(art_resp.read())
                job_settings["last_processed_id"] = job_id
        except Exception as e:
            print("Error downloading artifacts: " + str(e), file=sys.stderr)
            break
    if not ignore_settings:
        save_settings(GITLAB_HOST, GITLAB_PROJECT, GITLAB_JOB_NAME, job_settings, SETTINGS_FILE_NAME, GITLAB_TOKEN)
    sys.exit(0)

sys.exit(1)
